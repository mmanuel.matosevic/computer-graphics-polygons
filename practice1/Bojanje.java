package hr.fer.irg.lab2.practice1;

import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Bojanje {
    static{
        GLProfile.initSingleton();
    }

    public static boolean provjeri;

    public static void main(String[] args){
        List<iTocka2D> listTocka = new ArrayList<>();
        Map<Integer, iPolyElem> mapElem = new HashMap<>();
        List<iTocka2D> listProvjera = new ArrayList<>();

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                GLProfile glprofile = GLProfile.getDefault();  //Returns a default GLProfile object, reflecting the best for the running platform.
                GLCapabilities glcapabilities = new GLCapabilities(glprofile);  //Creates a GLCapabilities object.
                final GLCanvas glcanvas = new GLCanvas(glcapabilities);  //Creates a new GLCanvas component with the requested set of OpenGL capabilities, using the default OpenGL capabilities, provides OpenGL rendering support

                glcanvas.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        if(e.getButton() == 1){
                            System.out.println("x=" + e.getX() + " Y=" + e.getY());
                            listTocka.add(new iTocka2D(e.getX(), e.getY()));
                        }
                        if(listTocka.size() > 1){
                            provjeri = false;
                            glcanvas.display();
                        }
                        if(e.getButton() == 3){
                            listProvjera.add(new iTocka2D(e.getX(), e.getY()));
                            provjeri = true;
                            glcanvas.display();
                        }

                    }
                });



                glcanvas.addGLEventListener(new GLEventListener() {   //Adds the given listener to the end of this drawable queue. The listeners are notified of events in the order of the queue.

                    @Override
                    public void reshape(GLAutoDrawable glautodrawable, int x, int y, int width, int height) {   //Called by the drawable during the first repaint after the component has been resized.
                        GL2 gl2 = glautodrawable.getGL().getGL2(); //Returns the GL pipeline object this GLAutoDrawable uses.
                        gl2.glMatrixMode(GL2.GL_PROJECTION); //Sets the current matrix mode.
                        gl2.glLoadIdentity();  //Load the current matrix with the identity matrix

                        GLU glu = new GLU();  //Provides access to the OpenGL Utility Library (GLU). This library provides standard methods for setting up view volumes, building mipmaps and performing other common operations.
                        glu.gluOrtho2D(0.0f, width, 0.0f, height);

                        gl2.glMatrixMode(GL2.GL_MODELVIEW);  ////Sets the current matrix mode.
                        gl2.glLoadIdentity();  //Load the current matrix with the identity matrix

                        gl2.glViewport(0, 0, width, height); //Entry point to C language function: void glViewport
                    }

                    @Override
                    public void init(GLAutoDrawable glautodrawable) {  //Called by the drawable immediately after the OpenGL context is initialized.

                    }

                    @Override
                    public void dispose(GLAutoDrawable glautodrawable) {  //Notifies the listener to perform the release of all OpenGL resources per GLContext, such as memory buffers and GLSL programs.

                    }

                    @Override
                    public void	display(GLAutoDrawable glautodrawable) {  //Called by the drawable to initiate OpenGL rendering by the client.
                        GL2 gl2	= glautodrawable.getGL().getGL2();  //Returns the GL pipeline object this GLAutoDrawable uses.
                        int height = glautodrawable.getSurfaceHeight();

                        gl2.glClear(GL.GL_COLOR_BUFFER_BIT);  //clear buffers to preset values
                        gl2.glLoadIdentity();
                        if(listTocka.size() > 1){
                            racunajBrid(listTocka, mapElem, height);
                            int i0 = listTocka.size() - 1;
                            gl2.glBegin(GL.GL_POINTS);
                            for(int i = 0; i<listTocka.size(); i++){
                                bresenham(gl2, mapElem.get(i0).getVrh().getX(), height -mapElem.get(i0).getVrh().getY(), mapElem.get(i).getVrh().getX(), height - mapElem.get(i).getVrh().getY());
                                i0 = i;
                            }
                            if(listTocka.size() > 2){
                                popuniPoligon(gl2, mapElem, listTocka.size(), height);
                            }
                            gl2.glEnd();
                        }
                        if (listProvjera.size() > 0  && provjeri){
                            provjeriPoligon(mapElem, listProvjera.get(listProvjera.size()-1), height);
                        }

                    }
                });

                final JFrame jframe = new JFrame("Bojanje poligona i provjera tocke");
                jframe.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
                jframe.addWindowListener(new WindowAdapter() {

                    public void windowClosing(WindowEvent windowevent) {
                        jframe.dispose();
                        System.exit(0);
                    }
                });

                jframe.getContentPane().add(glcanvas, BorderLayout.CENTER);
                jframe.setSize(640, 480);
                jframe.setVisible(true);
                glcanvas.requestFocusInWindow();

            }
        });
    }

    public static void provjeriPoligon(Map<Integer, iPolyElem> mapElem, iTocka2D provjera, int height){
        boolean flag = false;
        for(int i=0; i<mapElem.size(); i++){
            int r = provjera.getX() * mapElem.get(i).getBrid().getA() +
                    (height - provjera.getY()) * mapElem.get(i).getBrid().getB() +
                    mapElem.get(i).getBrid().getC();
            if(r > 0){
                flag = true;
                break;
            }
        }
        if(flag){
            System.out.println("Tocka V je izvan poligona!");
        }
        else{
            System.out.println("Tocka V je unutar poligona");
        }
    }



    public static void popuniPoligon(GL2 gl2,Map<Integer, iPolyElem> mapElem, int n, int height){
        int i0, y;
        int xmin, xmax, ymin, ymax;

        double L, D, x;

        xmin = mapElem.get(0).getVrh().getX();
        xmax = mapElem.get(0).getVrh().getX();
        ymin = height - mapElem.get(0).getVrh().getY();
        ymax = height - mapElem.get(0).getVrh().getY();

        for(int i = 1; i < n; i++){
            if(xmin > mapElem.get(i).getVrh().getX()) xmin = mapElem.get(i).getVrh().getX();
            if(xmax < mapElem.get(i).getVrh().getX()) xmax = mapElem.get(i).getVrh().getX();
            if(ymin > height - mapElem.get(i).getVrh().getY()) ymin = height - mapElem.get(i).getVrh().getY();
            if(ymax < height - mapElem.get(i).getVrh().getY()) ymax = height - mapElem.get(i).getVrh().getY();
        }

        for(y = ymin; y<=ymax; y++){
            L = xmin;
            D = xmax;
            i0 = n - 1;

            for(int i = 0; i < n; i0 = i++){
                if(mapElem.get(i0).getBrid().getA() == 0){
                    if(height - mapElem.get(i0).getVrh().getY() == y){
                        if(mapElem.get(i0).getVrh().getX() < mapElem.get(i).getVrh().getX()){
                            L = mapElem.get(i0).getVrh().getX();
                            D = mapElem.get(i).getVrh().getX();
                        }
                        else{
                            L = mapElem.get(i).getVrh().getX();
                            D = mapElem.get(i0).getVrh().getX();
                        }
                        break;
                    }
                }
                else{
                    x = ((-mapElem.get(i0).getBrid().getB() * y) - mapElem.get(i0).getBrid().getC()) / (double) mapElem.get(i0).getBrid().getA();
                    if(mapElem.get(i0).getLijevi()){
                        if( L < x) L = x;
                    }
                    else{
                        if (D > x) D = x;
                    }
                }
            }
            bresenham(gl2, (int)L, y, (int)D, y);
        }
        System.out.println("Xmin=" + xmin + " Xmax=" + xmax + " Ymin=" + (height - ymax) + " Ymax=" + (height - ymin));
    }



    public static void racunajBrid(List<iTocka2D> listTocka, Map<Integer, iPolyElem> mapElem, int height){
        int i0 = listTocka.size() - 1;
        boolean lijevi;

        for(int i = 0; i < listTocka.size(); i++){
            int a = height - listTocka.get(i0).getY() - (height - listTocka.get(i).getY());
            int b = -(listTocka.get(i0).getX() - listTocka.get(i).getX());
            int c = listTocka.get(i0).getX() * (height - listTocka.get(i).getY()) - (height - listTocka.get(i0).getY()) * listTocka.get(i).getX();
            iBrid2D brid = new iBrid2D(a, b, c);
            if(height - listTocka.get(i0).getY() < height - listTocka.get(i).getY()){
                lijevi = true;
            }
            else{
                lijevi = false;
            }
            iPolyElem elem = new iPolyElem(listTocka.get(i0), brid, lijevi);
            mapElem.put(i0, elem);
            i0 = i;
        }
    }

    public static void bresenham(GL2 gl2, int xs, int ys, int xe, int ye){
        if(xs <= xe){
            if(ys <= ye){
                bresenham2(gl2, xs, ys, xe, ye);
            }
            else{
                bresenham3(gl2, xs, ys, xe, ye);
            }
        }
        else{
            if(ys >= ye){
                bresenham2(gl2, xe, ye, xs, ys);
            }
            else{
                bresenham3(gl2, xe, ye, xs, ys);
            }
        }
    }

    public static void bresenham2(GL2 gl2, int xs, int ys, int xe, int ye){
        int x, yc, korekcija;
        int a, yf;

        if(ye - ys <= xe - xs){
            a = 2*(ye-ys);
            yc = ys;
            yf = -(xe-xs);
            korekcija = -2*(xe-xs);
            for(x = xs; x <= xe; x++){
                gl2.glVertex2f(x, yc);
                yf = yf + a;
                if(yf >= 0){
                    yf = yf + korekcija;
                    yc = yc + 1;
                }
            }
        }
        else{
            x = xe;
            xe = ye;
            ye = x;
            x = xs;
            xs = ys;
            ys = x;
            a = 2 * (ye-ys);
            yc = ys;
            yf = -(xe-xs);
            korekcija = -2*(xe-xs);
            for(x = xs; x <= xe; x++){
                gl2.glVertex2f(yc, x);
                yf = yf + a;
                if(yf >= 0){
                    yf = yf + korekcija;
                    yc = yc + 1;
                }
            }
        }
    }

    public static void bresenham3(GL2 gl2, int xs, int ys, int xe, int ye){
        int x, yc, korekcija;
        int a, yf;

        if(-(ye-ys) <= xe-xs){
            a = 2*(ye-ys);
            yc = ys;
            yf = (xe-xs);
            korekcija = 2*(xe-xs);
            for(x = xs; x <= xe; x++){
                gl2.glVertex2f(x, yc);
                yf = yf + a;
                if(yf <=0){
                    yf = yf + korekcija;
                    yc = yc - 1;
                }
            }
        }
        else{
            x = xe;
            xe = ys;
            ys = x;
            x = xs;
            xs = ye;
            ye = x;
            a = 2*(ye - ys);
            yc = ys;
            yf = (xe - xs);
            korekcija = 2*(xe - xs);
            for(x = xs; x <= xe; x++){
                gl2.glVertex2f(yc, x);
                yf = yf + a;
                if(yf <= 0){
                    yf = yf + korekcija;
                    yc = yc - 1;
                }
            }
        }
    }



}
