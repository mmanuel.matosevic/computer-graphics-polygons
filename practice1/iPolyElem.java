package hr.fer.irg.lab2.practice1;

public class iPolyElem {
    private iTocka2D vrh;
    private iBrid2D brid;
    private boolean lijevi;

    public iPolyElem(iTocka2D vrh, iBrid2D brid, boolean lijevi){
        this.vrh = vrh;
        this.brid = brid;
        this.lijevi = lijevi;
    }

    public iTocka2D getVrh() {return this.vrh; }

    public iBrid2D getBrid() {return this.brid; }

    public boolean getLijevi() {return this.lijevi; }

    public void setLijevi(boolean lijevi) {this.lijevi = lijevi;}

    public String toString(){
        return "ELEM: " + vrh.toString() + "  " + brid.toString() + " LIJEVI = " + lijevi;
    }
}
