package hr.fer.irg.lab2.practice1;

public class iBrid2D {
    private int a;
    private int b;
    private int c;

    public iBrid2D(int a, int b, int c){
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public int getA() {return this.a;}

    public int getB() {return this.b;}

    public int getC() {return this.c;}

    public void setA(int a) {this.a = a;}

    public void setB(int b) {this.b = b;}

    public void setC(int c) {this.c = c;}

    public String toString(){
        return "BRID A=" + a + " B=" + b + " C=" + c;
    }
}
