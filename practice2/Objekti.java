package hr.fer.irg.lab2.practice2;

import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import jogamp.opengl.ListenerSyncedImplStub;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Objekti {
    static {
        GLProfile.initSingleton();
    }

    public static boolean flagScale = false;
    public static boolean keyPressed = false;
    public static iVrh3D vrh;
    public static float rotateFactor = 1;


    static GLProfile glprofile = GLProfile.getDefault();
    static GLCapabilities glcapabilities = new GLCapabilities(glprofile);
    static final GLCanvas glcanvas = new GLCanvas(glcapabilities);

    private static List<iPoligon> poligoniProvjera = new ArrayList<>();

    private static double x_max, x_min, y_max, y_min, z_max, z_min;


    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Upisite objekt koji zelite crtati: ");
        String path = "/home/mm51165/Desktop/Objekti/" + sc.next();
        sc.close();
        Path p = Path.of(path);
        List<String> listAll = Files.readAllLines(p);

        List<iVrh3D> listVrh = new ArrayList<>();
        poligoniProvjera = new ArrayList<>();
        for(String s : listAll){
            if(s.startsWith("v")){
                String[] string = s.split(" ");
                Double x = Double.parseDouble(string[1]);
                Double y = Double.parseDouble(string[2]);
                Double z = Double.parseDouble(string[3]);
                listVrh.add(new iVrh3D(x, y, z));
            }
            else if(s.startsWith("f")){
                String[] string = s.split(" ");
                Integer v1 = Integer.parseInt(string[1]);
                Integer v2 = Integer.parseInt(string[2]);
                Integer v3 = Integer.parseInt(string[3]);
                poligoniProvjera.add(new iPoligon(v1, v2, v3));
            }
        }

        nadiEkstreme(listVrh);


        SwingUtilities.invokeLater(new Runnable(){
            @Override
            public void run(){

                glcanvas.addMouseWheelListener(new MouseWheelListener() {
                    @Override
                    public void mouseWheelMoved(MouseWheelEvent mouseWheelEvent) {
                        int rotirano  = mouseWheelEvent.getWheelRotation();

                        rotateFactor = (float) (rotirano > 0? rotateFactor + 0.10 : rotateFactor - 0.10);

                        if (rotateFactor <= 0) rotateFactor = 1;



                        //flagScale = true;
                        glcanvas.display();
                    }
                });

                glcanvas.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mousePressed(MouseEvent e) {
                        vrh = new iVrh3D(e.getX(), e.getY(), 0);

                        keyPressed = true;
                        glcanvas.display();
                    }
                });


                glcanvas.addGLEventListener(new GLEventListener() {
                    @Override
                    public void init(GLAutoDrawable glAutoDrawable) {

                    }

                    @Override
                    public void dispose(GLAutoDrawable glAutoDrawable) {

                    }

                    @Override
                    public void reshape(GLAutoDrawable glautodrawable, int x, int y, int width, int height) {
                        GL2 gl2 = glautodrawable.getGL().getGL2(); //Returns the GL pipeline object this GLAutoDrawable uses.
                        gl2.glMatrixMode(GL2.GL_PROJECTION); //Sets the current matrix mode.
                        gl2.glLoadIdentity();  //Load the current matrix with the identity matrix

                        GLU glu = new GLU();  //Provides access to the OpenGL Utility Library (GLU). This library provides standard methods for setting up view volumes, building mipmaps and performing other common operations.
                        glu.gluOrtho2D(0.0f, width, height, 0.0f);

                        gl2.glMatrixMode(GL2.GL_MODELVIEW);  ////Sets the current matrix mode.
                        gl2.glLoadIdentity();  //Load the current matrix with the identity matrix

                        //gl2.glViewport(0, 0, width, height); //Entry point to C language function: void glViewport
                    }

                    @Override
                    public void display(GLAutoDrawable glautodrawable) {
                        GL2 gl2	= glautodrawable.getGL().getGL2();  //Returns the GL pipeline object this GLAutoDrawable uses.
                        int width = glautodrawable.getSurfaceWidth();
                        int height = glautodrawable.getSurfaceHeight();

                        gl2.glClear(GL.GL_COLOR_BUFFER_BIT);  //clear buffers to preset values


                        gl2.glLoadIdentity();  //Load the current matrix with the identity matrix
                        gl2.glTranslatef(width/2, height/2, 0.0f);
                        //gl2.glRotatef(45, 0, 0 ,1);


                        float faktor = Math.max(height, width);

                        float scale = (float) (faktor / Math.abs(x_max - x_min) * 0.6) * rotateFactor;


/*
                        if(flagScale){
                            scale = scale - mouseWheel.get(mouseWheel.size() - 1);
                            //gl2.glScalef(scale, scale, 1);
                            flagScale = false;
                        }

 */
                        gl2.glScalef(scale, scale, 0.0f);
                        if(keyPressed){
                            provjeri(vrh, listVrh, scale, height, width);
                            keyPressed = false;
                        }
                        gl2.glBegin(GL.GL_LINES);   //Specifies the primitive or primitives that will be created from vertices presented between glBegin and the subsequent glEnd.
                        for(iPoligon poligon : poligoniProvjera){
                            gl2.glVertex2f((float)listVrh.get(poligon.getV1() - 1).getX(), (float)listVrh.get(poligon.getV1() - 1).getY());
                            gl2.glVertex2f((float)listVrh.get(poligon.getV2() - 1).getX(), (float)listVrh.get(poligon.getV2() - 1).getY());
                            gl2.glVertex2f((float)listVrh.get(poligon.getV3() - 1).getX(), (float)listVrh.get(poligon.getV3() - 1).getY());
                        }
                        gl2.glEnd();
                    }
                });

                final JFrame jframe = new JFrame("Objekti");
                jframe.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
                jframe.addWindowListener(new WindowAdapter() {

                    public void windowClosing(WindowEvent windowevent) {
                        jframe.dispose();
                        System.exit(0);
                    }
                });

                jframe.getContentPane().add(glcanvas, BorderLayout.CENTER);
                jframe.setSize(550, 850);
                jframe.setVisible(true);
                glcanvas.requestFocusInWindow();
            }
        });

    }

    private static void nadiEkstreme(List<iVrh3D> vrhovi) {
        if (vrhovi.size() < 1) return;

        iVrh3D v = vrhovi.get(0);

        double xPom1 = v.getX(), yPom1 = v.getY(), zPom1 = v.getZ();
        double xPom2 = v.getX(), yPom2 = v.getY(), zPom2 = v.getZ();

        for (iVrh3D vrh : vrhovi) {
            if (vrh.getX() > xPom1) xPom1 = vrh.getX();
            if (vrh.getY() > yPom1) yPom1 = vrh.getY();
            if (vrh.getZ() > zPom1) zPom1 = vrh.getZ();

            if (vrh.getX() < xPom2) xPom2 = vrh.getX();
            if (vrh.getY() < yPom2) yPom2 = vrh.getY();
            if (vrh.getZ() < zPom2) zPom2 = vrh.getZ();
        }

        x_max = xPom1;
        x_min = xPom2;

        y_max = yPom1;
        y_min = yPom2;

        z_max = zPom1;
        z_min = zPom2;
    }

    public static void provjeri(iVrh3D vrh,List<iVrh3D> listVrh, float scale, int height, int width){
        List<iVrh3D> noviVrh = new ArrayList<>();
        for(iVrh3D vrh1 : listVrh){
            double x = vrh1.getX() * scale  +  + width/2.;
            double y = vrh1.getY() * scale +  height/2.;
            double z = vrh1.getZ() * scale;
            iVrh3D vrh3D = new iVrh3D(x, y, z);
            noviVrh.add(vrh3D);
        }

        List<iRavnina> pomocnaRavnina = new ArrayList<>();
        for(iPoligon poligon : poligoniProvjera){
            iVrh3D v1 = noviVrh.get(poligon.getV1() - 1);
            iVrh3D v2 = noviVrh.get(poligon.getV2() - 1);
            iVrh3D v3 = noviVrh.get(poligon.getV3() - 1);

            double A = (v2.getY()-v1.getY()) * (v3.getZ()-v1.getZ()) - (v2.getZ()-v1.getZ()) * (v3.getY()-v1.getY());
            double B = - (v2.getX()-v1.getX()) * (v3.getZ()-v1.getZ()) + (v2.getZ()-v1.getZ()) * (v3.getX()-v1.getX());
            double C = (v2.getX()-v1.getX()) * (v3.getY()-v1.getY()) - (v2.getY()-v1.getY()) * (v3.getX()-v1.getX());
            double D = - v1.getX()*A - v1.getY()*B - v1.getZ()*C;

            iRavnina ravnina = new iRavnina(A, B, C, D);
            pomocnaRavnina.add(ravnina);
        }



        boolean flag = true;
        for(iRavnina ravnina : pomocnaRavnina) {
            double ispitnaVrijednost = ravnina.getA()*vrh.getX() + ravnina.getB()*vrh.getY() + ravnina.getC()*0 + ravnina.getD();
            if(ispitnaVrijednost > 0) {
                flag = false;
                break;
            }
        }
        if(flag) System.out.println("Ispitna tocka nalazi se unutar tijela!");
        else System.out.println("Ispitna tocka nalazi se izvan tijela");

    }



    public static void bresenham(GL2 gl2, int xs, int ys, int xe, int ye){
        if(xs <= xe){
            if(ys <= ye){
                bresenham2(gl2, xs, ys, xe, ye);
            }
            else{
                bresenham3(gl2, xs, ys, xe, ye);
            }
        }
        else{
            if(ys >= ye){
                bresenham2(gl2, xe, ye, xs, ys);
            }
            else{
                bresenham3(gl2, xe, ye, xs, ys);
            }
        }
    }

    public static void bresenham2(GL2 gl2, int xs, int ys, int xe, int ye){
        int x, yc, korekcija;
        int a, yf;

        if(ye - ys <= xe - xs){
            a = 2*(ye-ys);
            yc = ys;
            yf = -(xe-xs);
            korekcija = -2*(xe-xs);
            for(x = xs; x <= xe; x++){
                gl2.glVertex2f(x, yc);
                yf = yf + a;
                if(yf >= 0){
                    yf = yf + korekcija;
                    yc = yc + 1;
                }
            }
        }
        else{
            x = xe;
            xe = ye;
            ye = x;
            x = xs;
            xs = ys;
            ys = x;
            a = 2 * (ye-ys);
            yc = ys;
            yf = -(xe-xs);
            korekcija = -2*(xe-xs);
            for(x = xs; x <= xe; x++){
                gl2.glVertex2f(yc, x);
                yf = yf + a;
                if(yf >= 0){
                    yf = yf + korekcija;
                    yc = yc + 1;
                }
            }
        }
    }

    public static void bresenham3(GL2 gl2, int xs, int ys, int xe, int ye){
        int x, yc, korekcija;
        int a, yf;

        if(-(ye-ys) <= xe-xs){
            a = 2*(ye-ys);
            yc = ys;
            yf = (xe-xs);
            korekcija = 2*(xe-xs);
            for(x = xs; x <= xe; x++){
                gl2.glVertex2f(x, yc);
                yf = yf + a;
                if(yf <=0){
                    yf = yf + korekcija;
                    yc = yc - 1;
                }
            }
        }
        else{
            x = xe;
            xe = ys;
            ys = x;
            x = xs;
            xs = ye;
            ye = x;
            a = 2*(ye - ys);
            yc = ys;
            yf = (xe - xs);
            korekcija = 2*(xe - xs);
            for(x = xs; x <= xe; x++){
                gl2.glVertex2f(yc, x);
                yf = yf + a;
                if(yf <= 0){
                    yf = yf + korekcija;
                    yc = yc - 1;
                }
            }
        }
    }


}
